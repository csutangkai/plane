package cn.mt.domain;

/**
 * @author mengdacheng on 2018/5/27.
 */
public class Rank {
    private String user;
    private int score;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
