package cn.mt.listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import cn.mt.enums.BulletEnum;
import cn.mt.ui.Airplane;
import cn.mt.ui.Container;

public class AirplaneListener implements KeyListener {

	private Container container;
	
	public AirplaneListener(Container container){
		this.container = container;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyPressed(KeyEvent e) {
		Airplane airplane = container.getAirplane();
		if (e.getKeyCode() == KeyEvent.VK_X) {
			int type = ((airplane.getBulletType().getCode() + 1) % 3) + 1;
			airplane.setBulletType(BulletEnum.getBulletById(type));
			return;
		}
		if (e.getKeyCode() == KeyEvent.VK_ENTER && container.isGameOver()) {
		    container.init();
		    return;
        }
		// 控制键盘监听事件
		airplane.setFlyStatus(e.getKeyCode());
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		Airplane airplane = container.getAirplane();
		airplane.setFlyStatus(0);
	}

}
