package cn.mt.enums;

/**
 * @author mengdacheng on 2018/5/24.
 */
public enum BulletEnum {
    NORMAL_BULLET(1, "image/bullet.png", 10, 20, 10),
    A_BULLET(2, "image/bullet_1.png", 30, 50, 20),
    B_BULLET(3, "image/bullet_3.png", 20, 50, 30),
    ENEMY_BULLET_A(11, "image/bullet2.png", 10, 20, 1),
    ENEMY_BULLET_B(12, "image/bullet_4.png", 10, 40, 1),
    UNKNOWN(-1, "image/bullet2.png", 10, 10, 1);

    private int code;
    private String img;
    private int width;
    private int height;
    private int hurt;
    BulletEnum (int code, String img, int width, int height, int hurt) {
        this.code = code;
        this.img = img;
        this.width = width;
        this.height = height;
        this.hurt = hurt;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHurt() {
        return hurt;
    }

    public void setHurt(int hurt) {
        this.hurt = hurt;
    }

    public static BulletEnum getBulletById(int id) {
        for (int i = 0; i < BulletEnum.values().length; i++) {
            if (BulletEnum.values()[i].getCode() == id) {
                return BulletEnum.values()[i];
            }
        }
        return BulletEnum.UNKNOWN;
    }
}
