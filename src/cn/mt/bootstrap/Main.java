package cn.mt.bootstrap;

import cn.mt.listener.AirplaneListener;
import cn.mt.ui.Container;

import javax.swing.*;

/**
 * @author mengdacheng on 2018/5/22.
 */
public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        //container加入bg
        Container container = new Container();

        // 初始化游戏界面
        container.init();

        //添加监听
        AirplaneListener al = new AirplaneListener(container);
        frame.addKeyListener(al);
        //frame里面加入container
        frame.add(container);
        frame.setSize(350, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);
    }
}
