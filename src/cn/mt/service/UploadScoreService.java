package cn.mt.service;

import cn.mt.domain.Rank;
import cn.mt.ui.Container;
import com.alibaba.fastjson.JSON;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @author mengdacheng on 2018/5/27.
 */
public class UploadScoreService implements Runnable {
    private static final int PORT = 8888;
    private Charset charset = Charset.forName("UTF-8");
    private Container container;

    public UploadScoreService(Container container) {
        this.container = container;
    }

    public void run() {
        try {
            InetSocketAddress isa = new InetSocketAddress("118.89.195.147", PORT);
            SocketChannel channel = SocketChannel.open(isa);
            String message = container.getUser() + ":" + container.getScore();
            channel.write(charset.encode(message));
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            StringBuilder content = new StringBuilder();
            int len = 0;
            while((len = channel.read(buffer)) > 0){
                buffer.flip();
                content.append(charset.decode(buffer));
            }
            if(len == -1){
                System.out.println("Connection Shutdown!");
                channel.close();
            }
            if (content.length() > 0) {
                List<Rank> ranks = JSON.parseArray(content.toString(), Rank.class);
                System.out.println("msg:" + ranks);
                container.setRanks(ranks);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
