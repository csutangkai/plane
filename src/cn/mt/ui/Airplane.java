package cn.mt.ui;

import cn.mt.constance.Rules;
import cn.mt.enums.BulletEnum;
import cn.mt.service.UploadScoreService;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

public class Airplane implements Shape {

    // 每帧飞机调整偏移量，影响飞机移动速度
    private static final int FRAME_OFFSET = 5;
    // 最大暴走时间
    private static final int MAX_RUNAWAY_TIME = 500;

    private Point position;
    private int offsetX;
    private int offsetY;
    private int width = 100;
    private int height = 100;
    private int flyStatus = 0;
    private int boomStatus = 0;
    private int refreshCount = 0;
    private Image image = new ImageIcon("image/plane.png").getImage();
    private BulletEnum bulletType = BulletEnum.NORMAL_BULLET;

    // 子弹发射速度
    private int bulletSpeed = 5;
    // 子弹每帧飞行的距离
    private int bulletDistance = 10;
    private int bulletLevel = 1;
    // 暴走时间
    private int runaway = 0;

    private List<Bullet> bulletList = new ArrayList<>();

    public Airplane(int x, int y) {
        this.position = new Point(x, y);
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public Point getPosition() {
        return this.position;
    }

    @Override
    public void setPosition(Point position) {
        this.position = position;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public void setOffsetY(int offsetY) {
        this.offsetY = offsetY;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }

    public void shootBullet() {
        // 一排中每个子弹的间隔=飞机的宽度/(子弹的个数+1)
        int step = width / (bulletLevel + 1);
        for (int i = 0; i < bulletLevel; i++) {
            // 需要对子弹的位置进行偏移调整，子弹的绘图位置=子弹的位置-(子弹的宽度/2)
            int bulletX = this.position.x + step * (i + 1) - bulletType.getWidth() / 2;
            Bullet bullet = new Bullet(bulletX, this.position.y);
            // 设置子弹类型
            bullet.setType(bulletType);

            if ((i < bulletLevel / 2 && bulletLevel % 2 != 0) || (bulletLevel % 2 == 0 && i < bulletLevel / 2 - 1)) {
                bullet.setAngle(-Math.PI / 20);
            } else if (i > bulletLevel / 2){
                bullet.setAngle(Math.PI / 20);
            }
            this.bulletList.add(bullet);
        }
    }
    //获取子弹List集合
    public List<Bullet> getBullet() {
        return this.bulletList;
    }

    /**
     * 刷新子弹
     */
    public void refreshBullet() {
        if (bulletLevel > 2) {
            runaway ++;
            if (runaway == MAX_RUNAWAY_TIME) {
                bulletLevel --;
                runaway = 0;
            }
        }

        refreshCount ++;
        if (refreshCount > 100 / bulletSpeed) {
            if (boomStatus == 0) {
                shootBullet();
            }
            refreshCount = 0;
        }
        int index = -1;
        for (int i = 0 ; i < bulletList.size(); i++) {
            Bullet bullet = bulletList.get(i);
            bullet.setPosition(new Point(bullet.getPosition().x + (int) (bulletDistance * Math.sin(bullet.getAngle())),bullet.getPosition().y - bulletDistance));
            if (bullet.getPosition().y < 0) {
                index = i;
            }
        }

        if (index > -1) {
            bulletList.remove(index);
        }
    }

    /**
     * 刷新飞机的位置
     */
    public void refreshPosition() {
        if (boomStatus > 0) {
            switch(boomStatus){
                case 10:
                    image = new ImageIcon("image/s1.png").getImage();break;
                case 20:
                    image = new ImageIcon("image/s2.png").getImage();break;
                case 30:
                    image = new ImageIcon("image/s3.png").getImage();break;
                case 40:
                    image = new ImageIcon("image/s4.png").getImage();break;
                case 50:
                    image = null;
            }
            boomStatus ++;
        } else {
            switch (flyStatus) {
                case KeyEvent.VK_UP:
                    if (position.y - FRAME_OFFSET > 0) {
                        position.y -= FRAME_OFFSET;
                    }
                    break;
                case KeyEvent.VK_DOWN:
                    if (position.y + FRAME_OFFSET < Rules.BOUNDARY_Y - this.height) {
                        position.y += FRAME_OFFSET;
                    }
                    break;
                case KeyEvent.VK_LEFT:
                    if (position.x - FRAME_OFFSET > 0) {
                        position.x -= FRAME_OFFSET;
                    }
                    break;
                case KeyEvent.VK_RIGHT:
                    if (position.x + FRAME_OFFSET < Rules.BOUNDARY_X - this.width) {
                        position.x += FRAME_OFFSET;
                    }
                    break;
            }
        }
    }

    public int getBulletSpeed() {
        return bulletSpeed;
    }

    public void setBulletSpeed(int bulletSpeed) {
        this.bulletSpeed = bulletSpeed;
    }

    public void setBulletDistance(int bulletDistance) {
        this.bulletDistance = bulletDistance;
    }

    public int getFlyStatus() {
        return flyStatus;
    }

    public void setFlyStatus(int flyStatus) {
        this.flyStatus = flyStatus;
    }

    /**
     * 飞机升级
     */
    public void upgrade() {
        if (bulletSpeed < 15) {
            bulletSpeed += 5;
        } else if (bulletLevel < 3){
            bulletLevel ++;
        }
    }

    public void setBoomStatus(int boomStatus) {
        this.boomStatus = boomStatus;
    }

    public int getBoomStatus() {
        return this.boomStatus;
    }

    public BulletEnum getBulletType() {
        return bulletType;
    }

    public void setBulletType(BulletEnum bulletType) {
        this.bulletType = bulletType;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
