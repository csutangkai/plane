package cn.mt.ui;

import cn.mt.enums.BulletEnum;

import javax.swing.*;

public class AEnemy extends Enemy {

	public AEnemy(int x, int y) {
		super(x, y);
	}

	public AEnemy(int x, int y, double radius) {
		super(x, y, radius);
	}

	@Override
	public void recover(){
		setImage(new ImageIcon("image/ae.png").getImage());
	}

	@Override
	public int beingHit (int hurt) {
		if (blood > 0) {
			this.blood -= hurt;
			this.image = new ImageIcon("image/a1.png").getImage();
			hurtTime = 10;
			if (blood <= 0) {
				this.setStatus(10);
				return 10;
			}
		}
		return 0;
	}

	@Override
	public void shootBullet() {
		Bullet bullet = new Bullet(this.position.x + 35, this.position.y + 80);
		bullet.setImage(BulletEnum.ENEMY_BULLET_A);
		this.bullets.add(bullet);
	}

	@Override
	/**
	 * 刷新飞机的位置
	 */
	public void move() {
		refreshBullet();
		position.y += 1;
		switch (getStatus()) {
			case 10:
				image = new ImageIcon("image/a1.png").getImage();
				break;
			case 20:
				image = new ImageIcon("image/a2.png").getImage();
				break;
			case 30:
				image = new ImageIcon("image/a3.png").getImage();
				break;
			case 40:
				image = new ImageIcon("image/a4.png").getImage();
				break;
		}
	}
}
