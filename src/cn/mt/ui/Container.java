package cn.mt.ui;

import cn.mt.constance.Rules;
import cn.mt.domain.Rank;
import cn.mt.service.UploadScoreService;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author mengdacheng on 2018/5/22.
 */
public class Container extends JPanel {

    private static final int FLUSH_FREQUENT = 100;

    private Random random = new Random(System.currentTimeMillis());
    private List<Rank> ranks = new ArrayList<>();

    // 背景
    private Background bg1 = new Background(0, 0);
    private Background bg2 = new Background(0, -Rules.BOUNDARY_Y);
    // 飞机
    private Airplane airplane;
    //补给品
    private List<Supply> supplies;
    //敌机List
    private List<Enemy> enemyList;

    private int score;
    private boolean gameOver;
    private String user = "meng";

    /**
     * 初始化游戏
     */
    public void init() {
        // 分数置0
        score = 0;
        enemyList = new ArrayList<>();
        supplies = new ArrayList<>();
        airplane = new Airplane(100, 480);
        gameOver = false;
        ranks = new ArrayList<>();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // 画背景
        g.drawImage(bg1.getImage(), 0, bg1.getPosition().y, null);
        g.drawImage(bg2.getImage(), 0, bg2.getPosition().y, null);
        // 画飞机
        g.drawImage(airplane.getImage(), airplane.getPosition().x, airplane.getPosition().y,  airplane.getWidth(), airplane.getHeight(), null);
        // 画子弹
        for (Bullet bullet : airplane.getBullet()) {
            g.drawImage(bullet.getImage(), bullet.getPosition().x, bullet.getPosition().y, bullet.getWidth(), bullet.getHeight(), null);
        }
        for (Supply shape : supplies) {
            g.drawImage(shape.getImage(), shape.getPosition().x, shape.getPosition().y, shape.getWidth(), shape.getHeight(), null);
        }

        for (Enemy enemy : enemyList) {
            Graphics2D g2 = (Graphics2D)g;
            g2.rotate(enemy.getRadius());
            g.drawImage(enemy.getImage(), enemy.getPosition().x, enemy.getPosition().y, enemy.getWidth(), enemy.getHeight(), null);
            if (airplane.getBoomStatus() == 0 && isCollision(enemy)) {
                airplane.setBoomStatus(10);
                new Thread(new UploadScoreService(this)).start();
            }
            // 判断飞机是否被击中
            for (Bullet bullet : enemy.getBullets()) {
                // 计算旋转后子弹的坐标
                int x = (int) (bullet.getPoint().x * Math.cos(enemy.getRadius())  - bullet.getPoint().y * Math.sin(enemy.getRadius()));
                int y = (int) (bullet.getPoint().x * Math.sin(enemy.getRadius()) + bullet.getPoint().y * Math.cos(enemy.getRadius()));
                g.drawImage(bullet.getImage(), bullet.getPosition().x, bullet.getPosition().y, bullet.getWidth(), bullet.getHeight(), null);
                if (airplane.getBoomStatus() == 0
                        && x  > airplane.getPosition().x + (airplane.getWidth() / 2) - 20
                        && x < airplane.getPosition().x + (airplane.getWidth() / 2) + 20
                        && y > airplane.getPosition().y
                        && y < airplane.getPosition().y + 50) {
                    airplane.setBoomStatus(10);
                    new Thread(new UploadScoreService(this)).start();
                }
            }
            g2.rotate(-enemy.getRadius());
        }
        g.setColor(Color.WHITE);
        g.setFont(new Font("宋体", Font.BOLD|Font.ITALIC, 25));
        g.drawString(String.valueOf(score), 10, 40);
        try {
            moveBackground();
            refreshSupply();
            airplane.refreshBullet();
            airplane.refreshPosition();
            if (airplane.getBoomStatus() > 50) {
                if (ranks.size() == 0) {
                    g.drawString("正在连接服务器...", 100, 200);
                } else {
                    g.drawString("Game Over!", 100, 200);
                    g.drawString("排行榜", 125, 250);
                    g.setFont(new Font("宋体", Font.BOLD | Font.ITALIC, 15));
                    int row = 0;
                    for (Rank rank : ranks) {
                        g.setColor(Color.WHITE);
                        g.drawString(rank.getUser(), 100, 300 + 20 * row);
                        if (rank.getUser().equals(user)) {
                            g.setColor(Color.RED);
                        }
                        g.drawString(String.valueOf(rank.getScore()), 200, 300 + 20 * row);
                        row++;
                    }
                    this.gameOver = true;
                }
            }
            hitEnemy();
            refreshAenemy();
            getSupply();
            Thread.sleep(1000 / FLUSH_FREQUENT);
            repaint();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 背景滚动
     */
    private void moveBackground() {
        bg1.setPosition(new Point(bg1.getPosition().x, bg1.getPosition().y + bg1.getMoveSpeed()));
        if (bg1.getPosition().y > Rules.BOUNDARY_Y - 50) {
            bg1.setPosition(new Point(bg1.getPosition().x, -Rules.BOUNDARY_Y));
        }
        bg2.setPosition(new Point(bg2.getPosition().x, bg2.getPosition().y + bg2.getMoveSpeed()));
        if (bg2.getPosition().y > Rules.BOUNDARY_Y - 50) {
            bg2.setPosition(new Point(bg2.getPosition().x, -Rules.BOUNDARY_Y));
        }
    }
    /**
     * 敌机被打中
     */
    private void hitEnemy(){
    	for(int i = 0; i < enemyList.size(); i++){
    		Enemy enemy = enemyList.get(i);
            // 计算旋转后飞机的位置
            int x = (int) (enemy.getPosition().x * Math.cos(enemy.getRadius())  - enemy.getPosition().y * Math.sin(enemy.getRadius()));
            int y = (int) (enemy.getPosition().x * Math.sin(enemy.getRadius()) + enemy.getPosition().y * Math.cos(enemy.getRadius()));
    		if (enemy.getStatus() == 0) {
                boolean isHit = false;
                for (int j = 0; j < airplane.getBullet().size(); j++) {
                    Bullet bullet = airplane.getBullet().get(j);

                    if (bullet.getPosition().y > y && bullet.getPosition().y < y + enemy.getHeight()
                            && bullet.getPosition().x > x && bullet.getPosition().x < x + enemy.getWidth()) {
                        score += enemy.beingHit(bullet.getHurt());
                        airplane.getBullet().remove(j);
                        isHit = true;
                    }
                }
                if (!isHit && enemy.getHurtTime() > 0) {
                    enemy.setHurtTime(enemy.getHurtTime() - 1);
                }
                // 指定帧数没有受到伤害即恢复敌机状态
                if (enemy.getHurtTime() < 1) {
                    enemy.recover();
                }
            }
    	}
    }
    /**
     * 获取补给
     */
    private void getSupply() {
        for (int i = 0; i < supplies.size(); ) {
            Supply supply = supplies.get(i);
            if (supply.getPosition().y < airplane.getPosition().y && supply.getPosition().y + 50 > airplane.getPosition().y
                    && supply.getPosition().x < airplane.getPosition().x + 58 && supply.getPosition().x + 50 > airplane.getPosition().x + 58) {
                supplies.remove(i);
                airplane.upgrade();
            } else {
                i ++;
            }
        }
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public void addSupply(Supply supply) {
        this.supplies.add(supply);
    }
	public void addAEnemy(Enemy aenemy) {
		this.enemyList.add(aenemy);
	}

    /**
     * 刷新补给
     */
    public void refreshSupply() {
        if (random.nextInt(1000) == 10) {
            supplies.add(new Supply(random.nextInt(300), 0));
        }
        for (Supply supply : supplies) {
            supply.setPosition(new Point(supply.getPosition().x, supply.getPosition().y + 1));
        }
    }
    /**
     * 刷新敌机状态
     */
    public void refreshAenemy(){
    	if (random.nextInt(100) == 10) {
    	    int sig = random.nextInt(2);
    	    double radius = Math.pow(-1, sig) * Math.PI / ((random.nextInt(5) + 1) * 10);
            enemyList.add(new BEnemy(random.nextInt(300), 0, radius));
        }

    	for(int i = 0; i < enemyList.size(); i++){
    		enemyList.get(i).move();
    		if(enemyList.get(i).getStatus() > 0 && enemyList.get(i).getStatus() < 50){
    			enemyList.get(i).setStatus(enemyList.get(i).getStatus() + 1);
    		}
    		if(enemyList.get(i).getStatus() == 50){
    			enemyList.remove(i);
    		}
    	}
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    /**
     * 判断是否和敌机发生碰撞
     * @param enemy
     * @return
     */
    private boolean isCollision(Enemy enemy) {
        int offset = 20;
        // 计算旋转后飞机的位置
        int x = (int) (enemy.getPosition().x * Math.cos(enemy.getRadius())  - enemy.getPosition().y * Math.sin(enemy.getRadius()));
        int y = (int) (enemy.getPosition().x * Math.sin(enemy.getRadius()) + enemy.getPosition().y * Math.cos(enemy.getRadius()));
//        x -= enemy.getWidth() / 2;
//        y -= enemy.getHeight() / 2;
//
//        int airplaneX = airplane.getPosition().x + airplane.getWidth() / 2;
//        int airplaneY = airplane.getPosition().y + airplane.getHeight() / 2;
//
//        if (Math.pow(x - airplaneX, 2) + Math.pow(y - airplaneY, 2) < offset) {
//            return true;
//        }

        if (x > airplane.getPosition().x + offset && x < airplane.getPosition().x + airplane.getWidth() - offset
                && y > airplane.getPosition().y + offset && y < airplane.getPosition().y + airplane.getHeight() - offset) {
            return true;
        }
        if (x + enemy.getWidth() > airplane.getPosition().x + offset && x + enemy.getWidth() < airplane.getPosition().x + airplane.getWidth() - offset
                && y > airplane.getPosition().y + offset && y < airplane.getPosition().y + airplane.getHeight() - offset) {
            return true;
        }
        if (x > airplane.getPosition().x + offset && x < airplane.getPosition().x + airplane.getWidth() - offset
                && y + enemy.getHeight() > airplane.getPosition().y + offset && y + enemy.getHeight() < airplane.getPosition().y + airplane.getHeight() - offset) {
            return true;
        }
        if (x + enemy.getWidth() > airplane.getPosition().x + offset && x + enemy.getWidth() < airplane.getPosition().x + airplane.getWidth() - offset
                && y + enemy.getHeight() > airplane.getPosition().y + offset && y + enemy.getHeight() < airplane.getPosition().y + airplane.getHeight() - offset) {
            return true;
        }
        return false;
    }

    public List<Rank> getRanks() {
        return ranks;
    }

    public void setRanks(List<Rank> ranks) {
        this.ranks = ranks;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
