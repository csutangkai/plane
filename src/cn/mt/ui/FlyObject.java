package cn.mt.ui;

import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public abstract class FlyObject extends JPanel {
	//飞机的高度和宽度
	int height;
	int weigth;
	//飞机的坐标
	int x;
	int y;
	//飞机的图片
	BufferedImage image;
	
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWeigth() {
		return weigth;
	}
	public void setWeigth(int weigth) {
		this.weigth = weigth;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
}
