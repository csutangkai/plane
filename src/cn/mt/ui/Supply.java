package cn.mt.ui;

import javax.swing.*;
import java.awt.*;

/**
 * @author mengdacheng on 2018/5/23.
 */
public class Supply implements Shape {
    private Point position;
    private int width = 30;
    private int height = 60;

    public Supply (int x, int y) {
        this.position = new Point(x, y);
    }

    @Override
    public Image getImage() {
        return new ImageIcon("image/supply.png").getImage();
    }

    @Override
    public Point getPosition() {
        return this.position;
    }

    @Override
    public void setPosition(Point position) {
        this.position = position;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
