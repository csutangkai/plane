package cn.mt.ui;

import javax.swing.*;
import java.awt.*;

/**
 * @author mengdacheng on 2018/5/22.
 */
public class Background implements Shape {

    private Point position;
    private int moveSpeed = 5;

    public Background(int x, int y) {
        position = new Point(x, y);
    }

    @Override
    public Image getImage() {
        return new ImageIcon("image/bg.png").getImage();
    }

    @Override
    public Point getPosition() {
        return this.position;
    }

    @Override
    public void setPosition(Point position) {
        this.position = position;
    }

    public int getMoveSpeed() {
        return moveSpeed;
    }

    public void setMoveSpeed(int moveSpeed) {
        this.moveSpeed = moveSpeed;
    }
}
