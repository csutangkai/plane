package cn.mt.ui;

import java.awt.*;

/**
 * @author mengdacheng on 2018/5/22.
 */
public interface Shape {
    Image getImage();
    Point getPosition();
    void setPosition(Point position);
}
