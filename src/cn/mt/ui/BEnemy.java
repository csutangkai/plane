package cn.mt.ui;

import cn.mt.enums.BulletEnum;

import javax.swing.*;

/**
 * @author mengdacheng on 2018/5/26.
 */
public class BEnemy extends Enemy{

    public BEnemy(int x, int y, double radius) {
        super(x, y);
        setRadius(radius);
//        setRadius(0);
        setImage(new ImageIcon("image/enemy/enemy_11.png").getImage());
        setWidth(60);
        setHeight(40);
    }

    @Override
    public void recover() {
        setImage(new ImageIcon("image/enemy/enemy_11.png").getImage());
    }

    @Override
    public int beingHit (int hurt) {
        if (getBlood() > 0) {
            setBlood(getBlood() - hurt);
            setImage(new ImageIcon("image/enemy/enemy_12.png").getImage());
            setHurtTime(10);
            if (getBlood() <= 0) {
                this.setStatus(10);
                return 10;
            }
        }
        return 0;
    }

    /**
     * 刷新飞机的位置
     */
    @Override
    public void move() {
        refreshBullet();
        position.y += 1;
        switch (getStatus()) {
            case 15:
                setImage(new ImageIcon("image/enemy/enemy_13.png").getImage());
                break;
            case 20:
                setImage(new ImageIcon("image/enemy/enemy_14.png").getImage());
                break;
            case 25:
                setImage(new ImageIcon("image/enemy/enemy_15.png").getImage());
                break;
            case 30:
                setImage(new ImageIcon("image/enemy/enemy_16.png").getImage());
                break;
            case 35:
                setImage(new ImageIcon("image/enemy/enemy_17.png").getImage());
                break;
            case 40:
                setImage(new ImageIcon("image/enemy/enemy_18.png").getImage());
                break;
        }
    }

    @Override
    public void shootBullet() {
        Bullet bullet = new Bullet(this.position.x + (this.getWidth() / 2), this.position.y);
        bullet.setType(BulletEnum.ENEMY_BULLET_B);
        this.bullets.add(bullet);
    }
}
