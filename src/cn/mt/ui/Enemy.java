package cn.mt.ui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Enemy implements Shape {
	
	protected int blood = 50;
	private int height = 80;
	private int width = 80;
	protected List<Bullet> bullets = new ArrayList<>();
	protected Point position;
	private int refreshCount;
	private int bulletSpeed = 1;
	private int bulletDistance = 5;
	private int status = 0;
	protected Image image = new ImageIcon("image/ae.png").getImage();
	protected int hurtTime = 0;
	private double radius;

	public Enemy(int x, int y) {
		this.position = new Point(x, y);
	}

	public Enemy(int x, int y, double radius) {
		this(x, y);
		this.radius = radius;
	}
	
	public int getBlood() {
		return blood;
	}
	public void setBlood(int blood) {
		this.blood = blood;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}

	public abstract void shootBullet();
	public abstract int beingHit (int hurt);
	public abstract void recover();
	public abstract void move();

	/**
	 * 刷新子弹
	 */
	public void refreshBullet() {
		refreshCount ++;
		if (refreshCount > 100 / bulletSpeed) {
			shootBullet();
			refreshCount = 0;
		}
		int index = -1;
		for (int i = 0 ; i < bullets.size(); i++) {
			Bullet bullet = bullets.get(i);
			bullet.setPosition(new Point(bullet.getPosition().x,bullet.getPosition().y + bulletDistance));
			if (bullet.getPosition().y > 1000) {
				index = i;
			}
		}

		if (index > -1) {
			bullets.remove(index);
		}
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public List<Bullet> getBullets() {
		return bullets;
	}

	public void setBullets(List<Bullet> bullets) {
		this.bullets = bullets;
	}

	@Override
	public Point getPosition() {
		return position;
	}

	@Override
	public void setPosition(Point position) {
		this.position = position;
	}

	@Override
	public Image getImage() {
		return image;
	}

	public int getRefreshCount() {
		return refreshCount;
	}

	public void setRefreshCount(int refreshCount) {
		this.refreshCount = refreshCount;
	}

	public int getBulletSpeed() {
		return bulletSpeed;
	}

	public void setBulletSpeed(int bulletSpeed) {
		this.bulletSpeed = bulletSpeed;
	}

	public int getBulletDistance() {
		return bulletDistance;
	}

	public void setBulletDistance(int bulletDistance) {
		this.bulletDistance = bulletDistance;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public int getHurtTime() {
		return hurtTime;
	}

	public void setHurtTime(int hurtTime) {
		this.hurtTime = hurtTime;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
}
