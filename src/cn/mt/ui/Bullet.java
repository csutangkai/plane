package cn.mt.ui;

import cn.mt.enums.BulletEnum;

import javax.swing.*;
import java.awt.*;

/**
 * @author mengdacheng on 2018/5/23.
 */
public class Bullet implements Shape {

    private Point position;
    private double angle;
    private Image image = new ImageIcon("image/bullet_1.png").getImage();
    private int width;
    private int height;
    private int hurt;
    // 子弹攻击点
    private Point point;

    public Bullet (int x, int y) {
        this.position = new Point(x, y);
    }

    @Override
    public Image getImage() {
        return image;
    }

    public void setImage(BulletEnum bulletEnum) {
        this.image = new ImageIcon(bulletEnum.getImg()).getImage();
    }

    @Override
    public Point getPosition() {
        return this.position;
    }

    @Override
    public void setPosition(Point position) {
        this.position = position;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHurt() {
        return hurt;
    }

    public void setHurt(int hurt) {
        this.hurt = hurt;
    }

    public void setType(BulletEnum bulletEnum) {
        this.width = bulletEnum.getWidth();
        this.height = bulletEnum.getHeight();
        this.image = new ImageIcon(bulletEnum.getImg()).getImage();
        this.hurt = bulletEnum.getHurt();
    }

    public Point getPoint () {
        int x = position.x + width / 2;
        int y = position.y + height / 2;
        return new Point(x, y);
    }
}
